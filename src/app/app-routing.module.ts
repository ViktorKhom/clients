import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientsListComponent } from './components/clients-list/clients-list.component';
import { CreateClientComponent } from './components/create-client/create-client.component';

const routes:Routes = [
    { path:'', component:ClientsListComponent },
	{ path:'create-client', component:CreateClientComponent },
];

@NgModule({
	imports:[
		RouterModule.forRoot(routes)
	],
	exports:[
		RouterModule
	]
})

export class AppRoutingModule {

}