import { Component, OnInit, Input  } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ClientService } from '../../services/client.service';
import * as moment from 'moment';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  @Input() clientItem: any;
  @Input() clientItemIndex: number;
  private transformedClientItem: any;
  private keysOftransformedClientItem: any[] = [];
  private form: FormGroup;
  private editMode: string = 'editModeFor-NULL';

  constructor(private clientService: ClientService){
  }

  ngOnInit() {
    this.updateClientItem();
    this.clientService.dataSource.subscribe(data => { this.updateClientItem();});
  }

  updateClientItem() {
    if(this.clientItem) {
      let formGroup = {};
      this.transformedClientItem = {...this.clientItem};
      if(this.transformedClientItem['FIRSTNAME']) {
        this.transformedClientItem['FIRSTNAME'] = this.clientItem['FIRSTNAME'] + ' ' + this.clientItem['LASTNAME'] + ' ' + this.clientItem['PATRONYMIC'];
      }
      if(this.transformedClientItem['BIRTHDATE']) {
        this.transformedClientItem['BIRTHDATE'] = moment(new Date(this.transformedClientItem['BIRTHDATE']).toISOString()).format('DD/M/YYYY');
      } 
      let {LASTNAME, PATRONYMIC, ...newClientItem} = this.transformedClientItem;
      this.transformedClientItem = newClientItem;
      this.keysOftransformedClientItem = Object.keys(this.transformedClientItem);
      for (var prop in this.transformedClientItem) {
        prop === 'BIRTHDATE' ? formGroup[prop] = new FormControl(this.transformedClientItem[prop], 
                               [Validators.required, Validators.pattern("(([1-2][0-9])|([1-9])|(3[0-1]))/((1[0-2])|([1-9]))/[0-9]{4}")]) :
                               formGroup[prop] = new FormControl(this.transformedClientItem[prop], [Validators.required, Validators.minLength(2)]);
      }
      this.form = new FormGroup(formGroup);
    }
  }

  checkFieldState(key: string) {
    if(this.form.invalid) {
      this.cancelEditing(key);
    }
  }

  editClient(key) {
    this.editMode = this.editMode.split('-')[0] + '-' + key;
  }

  saveEditedClient(key: string) {
    this.clientService.editClientStroke(this.clientItemIndex, this.transformedClientItem, key, this.form.controls[key].value);
    this.cancelEditing(key);
  }

  removeClientStroke(key: string) {
    this.clientService.removeClientStroke(this.clientItemIndex, key);
  }

  removeClientFromList() {
    this.clientService.removeClient(this.clientItemIndex);
  }

  cancelEditing(key: string) {
    this.form.controls[key].setValue(this.transformedClientItem[key]);
    this.editMode = 'editModeFor-NULL';
  }

}
