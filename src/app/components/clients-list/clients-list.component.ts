import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../services/client.service';
import { ClientComponent } from '../client/client.component';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss']
})
export class ClientsListComponent implements OnInit {

  private clientsList: any = [];

  constructor(private clientService: ClientService){}

  ngOnInit(){
    this.clientService.initAndTransformClientsList();
    this.clientsList = this.clientService.getClients();
    this.clientService.dataSource.subscribe(data => { this.clientsList = data; });
  }

}
