import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-create-client',
  templateUrl: './create-client.component.html',
  styleUrls: ['./create-client.component.scss']
})
export class CreateClientComponent implements OnInit {
  private form: FormGroup;
  constructor(private clientService: ClientService){
  }

  ngOnInit() {
    this.form = new FormGroup({
      'CARD': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'LASTNAME': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'FIRSTNAME': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'PATRONYMIC': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'IDPERSON': new FormControl(Math.floor(1 + Math.random() * (9 + 1 - 1))),
      'STATUSNAME': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'ACC1NUM': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'CARDTEMPLNAME': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'ORGNAMESHORT': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'PERSONTYPENAME': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'CURRNAME': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'IDCARD': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'BIRTHDATE': new FormControl('', [Validators.required, Validators.pattern("(([1-2][0-9])|([1-9])|(3[0-1]))/((1[0-2])|([1-9]))/[0-9]{4}")]),
      'DOCSERIES': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'DOCNUM': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'IDTASKAUTHSTATUS': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'DOCTYPENAME': new FormControl('', [Validators.required, Validators.minLength(2)]),
    });
  }

  sendNewClient() {
    const newClient = this.form.value;
    this.clientService.addNewClient(newClient);
  }

}
