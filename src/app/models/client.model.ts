export class Client {
    constructor(
      public CARD: string,
      public LASTNAME: string,
      public FIRSTNAME: string,
      public PATRONYMIC: string,
      public IDPERSON: number,
      public STATUSNAME: string,
      public ACC1NUM: number,
      public CARDTEMPLNAME: string,
      public ORGNAMESHORT: string,
      public PERSONTYPENAME: string,
      public CURRNAME: string,
      public IDCARD: number,
      public BIRTHDATE: Date,
      public DOCSERIES: string,
      public DOCNUM: string,
      public IDTASKAUTHSTATUS: number,
      public DOCTYPENAME: string
    ) {}
  }
  