import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';

export class ClientService {
  private clientsList: any = [];
  private REST_API_SERVER = "http://localhost:4200/assets/data/person.json";
  dataSource:Subject<any> = new Subject();
  constructor(private http: HttpClient, private router: Router) {
  }

  initAndTransformClientsList() {
    if(this.clientsList.length === 0) {
      this.http.get(this.REST_API_SERVER).subscribe((response: any)=>{
        for(let i = 0; i < response.data.rows.length; i++) {
          let newRowItem = {};
          response.data.rows[i].map((rowItem, index) => {
            newRowItem[response.data.metaData[index].name] = rowItem;
          });
          this.clientsList.push(newRowItem);
        }
        this.saveClients(this.clientsList);
      });
    }
  }

  getClients() {
    return this.clientsList;
  }

  addNewClient(newClient:{}) {
    this.clientsList.push(newClient);
    this.saveClients(this.clientsList);
    this.router.navigateByUrl('');
  }

  saveClients(clientsList:any[]) {
    this.dataSource.next(clientsList);
  }

  editClientStroke(index:number, item:{}, key:string, newValue:any) {
    if(key === 'FIRSTNAME') {
      newValue.split(' ')[0] ? this.clientsList[index]['FIRSTNAME'] = newValue.split(' ')[0] : this.clientsList[index]['FIRSTNAME'] = '';
      newValue.split(' ')[1] ? this.clientsList[index]['LASTNAME'] = newValue.split(' ')[1] : this.clientsList[index]['LASTNAME'] = '';
      newValue.split(' ')[2] ? this.clientsList[index]['PATRONYMIC'] = newValue.split(' ')[2] : this.clientsList[index]['PATRONYMIC'] = '';
    }
    else {
      this.clientsList[index][key] = newValue;
    }
    this.saveClients(this.clientsList);
  }

  removeClientStroke(index: number, key: string) {
    if(key === 'FIRSTNAME') { 
      delete this.clientsList[index]['FIRSTNAME'];
      delete this.clientsList[index]['LASTNAME'];
      delete this.clientsList[index]['PATRONYMIC'];
    }
    else {
      delete this.clientsList[index][key];
    }
    this.saveClients(this.clientsList);
  }

  removeClient(index:number) {
    let filteredArray = this.clientsList.filter((client, ind) => ind !== index);
    this.saveClients(filteredArray);
  }

}
